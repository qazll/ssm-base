package com.example.demo.api.controller.v1;


import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * API接口
 */
@Api(tags = "测试接口")
@RestController("exampleController.v1")
@RequestMapping("/v1/example")
public class ExampleController{

    }



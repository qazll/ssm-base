package com.example.demo.api.controller.v1;


import com.example.demo.app.service.ISysUserService;
import com.example.demo.domain.entity.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author YangMengMeng
 * @since 2021-07-27
 */
@RestController
@RequestMapping("/demo/sys-user")
public class SysUserController {

    @Autowired(required = false)
    private ISysUserService sysUserService;

    @GetMapping
    public Object list(){
        return sysUserService.list();
    }

    @PostMapping
    public boolean save(){
        return sysUserService.saveAsAdmin(new SysUser("ymm","dwd",23,"dwdw"));
    }


}

package com.example.demo.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *  用户实体类
 * </p>
 *
 * @author YangMengMeng
 * @since 2021-07-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_user")
@ApiModel(value="SysUser对象", description="用户实体类")
public class SysUser implements Serializable {

    private static final long serialVersionUID = 1237482334237236L;

    public SysUser() {
    }

    public SysUser(String username, String information, Integer age, String userDesc) {
        this.username = username;
        this.information = information;
        this.age = age;
        this.userDesc = userDesc;
    }

    @ApiModelProperty(value = "主键")
    @TableId(value = "user_id", type = IdType.AUTO)
    private Long userId;

    @ApiModelProperty(value = "用户名")
    @TableField("username")
    private String username;

    @ApiModelProperty(value = "信息")
    @TableField("information")
    private String information;

    @ApiModelProperty(value = "年龄")
    @TableField("age")
    private Integer age;

    @ApiModelProperty(value = "备注")
    @TableField("user_desc")
    private String userDesc;


}

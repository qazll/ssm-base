package com.example.demo;

import org.mapstruct.BeanMapping;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 * 启动类，
 * 记得添加@MapperScan哦
 * </p>
 * 
 * @author YangMengMeng 2021/07/27 10:35
 */

@MapperScan("com.example.demo.infra.mapper")
@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

}

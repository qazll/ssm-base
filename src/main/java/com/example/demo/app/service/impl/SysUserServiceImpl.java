package com.example.demo.app.service.impl;

import javax.annotation.Resource;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.example.demo.domain.entity.SysUser;
import com.example.demo.infra.mapper.SysUserMapper;
import com.example.demo.app.service.ISysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.font.TrueTypeFont;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author YangMengMeng
 * @since 2021-07-27
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {


    @Resource
//    @Autowired(required = false)
    SysUserMapper sysUserMapper;

    /**
     * 事务测试
     *
     * @author YangMengMeng 2021-07-29 10:16
     * @return bool
     */
    @Override
    @Transactional(
            rollbackFor = Exception.class
    )
    public boolean saveAsAdmin(SysUser user){
        sysUserMapper.insert(user);
        if(true){
            throw new RuntimeException("抛出异常了");
        }
//        sysUserMapper.delete(user);

        return true;
    }
}

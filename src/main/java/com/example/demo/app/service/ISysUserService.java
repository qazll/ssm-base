package com.example.demo.app.service;

import com.example.demo.domain.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author YangMengMeng
 * @since 2021-07-27
 */
public interface ISysUserService extends IService<SysUser> {

    /**
     *  保存用户
     *
     * @param  user
     * @author YangMengMeng 2021-07-29 9:54
     * @return boolean
     */
    public boolean saveAsAdmin(SysUser user);
}

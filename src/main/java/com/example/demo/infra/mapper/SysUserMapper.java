package com.example.demo.infra.mapper;

import com.example.demo.domain.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author YangMengMeng
 * @since 2021-07-27
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     *
     *  删除
     * @author YangMengMeng 2021-07-29 16:25
     */
    void delete(SysUser user);
}

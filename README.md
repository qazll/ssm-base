# SSM-base

#### 介绍
SpringBoot+Mybatis-plus开发模板含代码生成器

#### 开发环境
1. jdk 8
2. mysql 8



#### 数据库表

##### 数据库配置
url: jdbc:mysql://127.0.0.1:3306/demo?useUnicode=true&characterEncoding=utf-8&useSSL=false
username: root
password: 123

##### 数据库表

###### sys_user表

CREATE TABLE `sys_user` (
`user_id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
`username` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
`information` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '信息',
`age` int DEFAULT NULL COMMENT '年龄',
`user_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注',
PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;



#### 参与贡献

1.  Fork 本仓库
2.  新建 dev_xxx 分支
3.  提交代码
4.  新建 Pull Request


